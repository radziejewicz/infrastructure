# NixOS

## Im running nix, how do I use the code in this repo?

Create or alter the following files:
```nix
# create /home/me/secrets.nix
{ config, pkgs, ... }:
{
  config.gitlab.email = "";
  config.gitlab.serialNumber = "";
  config.gitlab.sentinelOneManagementToken = "";
}

# Add to /etc/nixos/configuration.nix
{
    imports = [
        /home/me/secrets.nix
    ];
}

# Add to or create /etc/nixos/flake.nix
{
  description = "<NAME>'s NixOS Configurations";
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/f28c957a927b0d23636123850f7ec15bda9aa2f4";
    };
    reliability.url  = "gitlab:gitlab-com/gl-infra/reliability/e020b591d6fb92be06ddae94b80c3616e8344bc7"; 
  };

  outputs = { self, nixpkgs, reliability }@inputs: rec {
    legacyPackages = nixpkgs.lib.genAttrs [ "x86_64-linux" ] (system:
      import inputs.nixpkgs {
        inherit system;

        # unfree packages
        config.allowUnfree = true;
      }
    );
    nixosConfigurations = {
      # Work laptop
      codeine = nixpkgs.lib.nixosSystem {
        pkgs = legacyPackages.x86_64-linux;
        system  = "x86_64-linux";
        modules = [
          ./configuration.nix # Your local configuration
          reliability.nixosModules.sre
        ];
      };
      # Add other machines here
    };
  };
}

```
A complete example can be found here:
https://gitlab.com/knottos/nixos-configs

