{
  description = "GitLab SRE for Nix";
  inputs = {};
  outputs = { self, ... }: rec {
    nixosModules = rec {
      sre = import ./nix;
      default = sre;
    };
  };
}